import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CharactersPageComponent } from './pages/characters-page/characters-page.component';
import { LocationsPageComponent } from './pages/locations-page/locations-page.component';
import { ContacPageComponent } from './pages/contac-page/contac-page.component';
import { FavoritesPageComponent } from './pages/favorites-page/favorites-page.component';
import { CharactersDetailPageComponent } from './pages/characters-page/pages/characters-detail-page/characters-detail-page.component';

const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'characters', component: CharactersPageComponent},
  {path: 'characters/:idCharacter', component: CharactersDetailPageComponent},
  {path: 'locations', component: LocationsPageComponent},
  {path: 'favorites', component: FavoritesPageComponent},
  {path: 'contact', component: ContacPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
