import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CharactersPageComponent } from './pages/characters-page/characters-page.component';
import { NavComponent } from './core/components/nav/nav.component';
import { HttpClientModule } from '@angular/common/http';
import { GalleryComponent } from './shared/components/gallery/gallery.component';
import { LocationsPageComponent } from './pages/locations-page/locations-page.component';
import { FavoritesPageComponent } from './pages/favorites-page/favorites-page.component';
import { ContacPageComponent } from './pages/contac-page/contac-page.component';
import { FormsModule, NgModel, ReactiveFormsModule } from '@angular/forms';
import { CharactersDetailPageComponent } from './pages/characters-page/pages/characters-detail-page/characters-detail-page.component';
import { MultiplyPipe } from './shared/pipes/multiply.pipe';
import { PriorityNamePipe } from './shared/pipes/priority-name.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    CharactersPageComponent,
    NavComponent,
    GalleryComponent,
    LocationsPageComponent,
    FavoritesPageComponent,
    ContacPageComponent,
    CharactersDetailPageComponent,
    MultiplyPipe,
    PriorityNamePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
