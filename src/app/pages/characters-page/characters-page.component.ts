import { Component, OnInit } from '@angular/core';
import { CharactersService } from 'src/app/shared/services/characters.service';

@Component({
  selector: 'app-characters-page',
  templateUrl: './characters-page.component.html',
  styleUrls: ['./characters-page.component.scss'],
})
export class CharactersPageComponent implements OnInit {
  characters: any;
  characterName= "";
  actualPage = 1;

  constructor(private charactersService: CharactersService) {}

  ngOnInit(): void {

    this.filterCharacters('', 1);
  }
  filterCharacters(characterName:any, actualPage:any) {
    this.actualPage = actualPage;
    this.charactersService.getCharacters(characterName.toLowerCase(),actualPage).subscribe((data: any) => {
      this.characters = data.results;
      console.log(this.characters);
  })
  }
}
