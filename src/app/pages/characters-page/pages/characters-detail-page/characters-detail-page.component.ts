import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharactersService } from 'src/app/shared/services/characters.service';

@Component({
  selector: 'app-characters-detail-page',
  templateUrl: './characters-detail-page.component.html',
  styleUrls: ['./characters-detail-page.component.scss']
})
export class CharactersDetailPageComponent implements OnInit {

  findCharacter: any;
  character:any ;

  constructor(private charactersService:CharactersService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.findCharacter = params.get('idCharacter');

      this.charactersService.getCharacter(this.findCharacter).subscribe((data:any) => {
        this.character = data;
      })
    })
  }

}
