import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-contac-page',
  templateUrl: './contac-page.component.html',
  styleUrls: ['./contac-page.component.scss']
})
export class ContacPageComponent implements OnInit {

  contactForm: any;
  submitted = false;
  constructor(private formBuilder: FormBuilder) { 
    this.contactForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', [Validators.required, Validators.email]],
      message: ['', [Validators.required, Validators.minLength(10)]],
      isHuman: [false, [Validators.requiredTrue]]
    })
  }

  ngOnInit(): void {
  }

  submitForm(){
    this.submitted = true;
    console.log(this.contactForm);
    console.log(this.contactForm.value);
  }

}
