import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'multiply'
})
export class MultiplyPipe implements PipeTransform {

  transform(value: number, arg:any, arg1:any): number {
    // return value * 2; Si quisiera multiplicar siempre por 2
    return value * arg + arg1;
  }

}
