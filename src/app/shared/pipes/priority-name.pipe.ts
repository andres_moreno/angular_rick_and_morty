import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'priorityName'
})
export class PriorityNamePipe implements PipeTransform {

  transform(value: any, arg1:any, arg2:any): any {
    if(value.includes('Rick')){
      return arg1 + value + arg2;
    }
    return value;
  
  }
}
