import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  constructor(private http: HttpClient) { }

  getCharacters(name:any, page=1){
    return this.http.get('https://rickandmortyapi.com/api/character?page=' + page ​​+ '&name=' +​​name)
  }

  getCharacter(id:any){
    return this.http.get('https://rickandmortyapi.com/api/character/' + id)
  }
}
