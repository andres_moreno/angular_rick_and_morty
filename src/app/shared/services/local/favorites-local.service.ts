import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FavoritesLocalService {

  private favorites: any = [];

  constructor() { }

  addFavorite(newFavorite:any){
    this.favorites.push(newFavorite);
  }

  getFavorite(){
    return this.favorites;
  }
}
